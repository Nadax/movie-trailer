const cp = require('child_process')
const { join } = require('path')


async function run() {
  const script = join(__dirname, '../crawler/trailer.js')
  const child = cp.fork(script, [])
  let invoked = false

  child.on('error', (err) => {
    if (invoked) return
    invoked = true
    console.log(err)
  })

  child.on('exit', (code) => {
    if (invoked) return
    invoked = true
    const err = code === 0 ? null : new Error('exit code ' + code)
    console.log(err)
  })

  child.on('message', (data) => {
    const result = data.result
    console.log(result)
  })

}

run()
