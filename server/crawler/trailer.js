const puppeteer = require('puppeteer')

const id = '30287738'
const subjectUrl = 'https://movie.douban.com/subject/'

async function run() {
  const browser = await puppeteer.launch({
    args: ['--no-sandbox'],
    dumpio: false,
    headless: false
  })
  const page = await browser.newPage()

  // 跳转到电影详情页面
  await page.goto(subjectUrl + id, {
    waitUntil: 'networkidle2'
  })
  await page.waitFor(1000)

  // 爬取预告片信息(链接, 封面)
  const trailer = await page.evaluate(() => {
    const $item = $('.related-pic-video')
    if ($item.length > 0) {
      const link = $item.attr('href')
      const cover = $item.css('background-image').replace('url("', '').replace('")', '')
      return {
        link,
        cover
      }
    }
  })

  // 爬取预告片
  let video = ''
  if (trailer.link) {
    await page.goto(trailer.link, {
      waitUntil: 'networkidle2'
    })
    await page.waitFor(1000)
    video = await page.evaluate(() => {
      const $item = $('video > source')
      if ($item.length > 0) {
        return $item.attr('src')
      } else {
        return ''
      }
    })
  }

  const result = {
    id,
    video,
    cover: trailer.cover
  }

  browser.close()
  process.send({result})
  process.exit(0)
}

run()

