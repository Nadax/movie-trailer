const puppeteer = require('puppeteer')

const url = 'https://movie.douban.com/tag/#/?sort=R&range=7,10&tags=%E7%94%B5%E5%BD%B1'
const MORE_SELECTOR = '#app > div > div.article > a'

async function run() {
  const browser = await puppeteer.launch({
    args: ['--no-sandbox'],
    dumpio: false,
    headless: false
  })
  const page = await browser.newPage()

  await page.goto(url, {
    waitUntil: 'networkidle2'
  })
  for (let i = 0; i < 1; i++) {
    await page.waitForSelector(MORE_SELECTOR)
    await page.click(MORE_SELECTOR)
    await page.waitFor(1000)
  }

  const result = await page.evaluate(() => {
    const $items = $('.list-wp a')
    const links = []
    if ($items.length > 0) {
      $items.each((index, item) => {
        const $item = $(item)
        const id = $item.find('div').data('id')
        const title = $item.find('.title').text()
        const rate = parseFloat($item.find('.rate').text())
        const poster = $item.find('.pic > img').attr('src').replace('s_ratio', 'l_ratio')
        links.push({ id, title, rate, poster })
      })
    }
    return links
  })

  browser.close()
  process.send({result})
  process.exit(0)
}


run()
